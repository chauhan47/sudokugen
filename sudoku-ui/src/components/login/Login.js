import React, { useState } from "react";
import { Link, useHistory } from "react-router-dom";

const Login = () => {
    const [loginCredentials, setLoginCredentials] = useState({
        username: '',
        password: ''
    })
    const hisroty = useHistory();

    const handleInputChange = (e) => {
        setLoginCredentials({ ...loginCredentials, [e.target.id]: e.target.value })
    }

    const submitHandler = async (e) => {
        e.preventDefault();
        if (loginCredentials.username && loginCredentials.password) {
            const requestOptions = {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify({ loginCredentials })
            };
            await fetch(`http://localhost:5000/user/signin`, requestOptions)
                .then(response => response.json())
                .then(data => {
                    if (data.response[0] === 200) {
                        localStorage.setItem("userToken", data.response[1].token)
                        hisroty.push('/play')
                    } else {
                        alert('Invalid username/password')
                    }
                })
        }
    }

    return (
        <div className="w-40 m-auto" style={{ width: '40%' }}>
            <form onSubmit={submitHandler}>
                <div class="form-group">
                    <label for="exampleInputEmail1">Username</label>
                    <input
                        type="username"
                        className="form-control"
                        id="username"
                        aria-describedby="emailHelp"
                        // value={email}
                        onChange={(e) => handleInputChange(e)} />
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Password</label>
                    <input
                        type="password"
                        className="form-control"
                        id="password"
                        aria-describedby="emailHelp"
                        // value={email}
                        onChange={(e) => handleInputChange(e)} />
                </div>
                <Link to={`/register`} aria-hidden="true">Register</Link>
                <button type="submit" class="btn btn-primary my-2 btn-sm" style={{ float: 'right' }}>Submit</button>
            </form>
        </div>
    )
}

export default Login;