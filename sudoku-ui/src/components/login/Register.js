import React, { useState } from "react";
import { Link } from "react-router-dom";

const Register = () => {
    const [userData, setUserData] = useState({
        fullname: '',
        username: '',
        password: ''
    })

    const handleInputChange = (e) => {
        setUserData({ ...userData, [e.target.id]: e.target.value })
    }

    const submitHandler = async (e) => {
        e.preventDefault();
        if (userData.username && userData.password && userData.fullname) {
            const requestOptions = {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify({ userData })
            };
            await fetch(`http://localhost:5000/user/register`, requestOptions)
                .then(response => response.json())
                .then(data => alert("Registered"));
        }
    }

    return (
        <div className="w-40 m-auto" style={{ width: '40%' }}>
            <form onSubmit={submitHandler}>
            <div class="form-group">
                    <label for="exampleInputEmail1">Full Name</label>
                    <input
                        type="text"
                        className="form-control"
                        id="fullname"
                        value={userData.fullname}
                        onChange={(e) => handleInputChange(e)} />
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Username</label>
                    <input
                        type="text"
                        className="form-control"
                        id="username"
                        value={userData.username}
                        onChange={(e) => handleInputChange(e)} />
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Password</label>
                    <input
                        type="password"
                        className="form-control"
                        id="password"
                        aria-describedby="emailHelp"
                        value={userData.password}
                        onChange={(e) => handleInputChange(e)} />
                </div>

                <Link to={`/login`} aria-hidden="true">Login</Link>
                <button type="submit" class="btn btn-primary my-2 btn-sm" style={{float: 'right'}}>Submit</button>
            </form>
        </div>
    )
}

export default Register;