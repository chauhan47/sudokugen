import React, { useEffect, useState } from "react";

const UserInfo = () => {
    const [userInfo, setUserInfo] = useState({
        fullName: '',
        score: 0,
    });

    useEffect(() => {
        const token = localStorage.getItem('userToken')
        const requestOptions = {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`,
            }
        };
        fetch('http://localhost:5000/user/info', requestOptions)
            .then(response => response.json())
            .then(data => {
                setUserInfo({ fullName: data.response.fullName, score: data.response.score })
            });
    }, [])


    return (
        <>
            <form style={{ width: '20vw' }}>
                <div class="form-group row">
                    <label class="col-sm-4 col-form-label col-form-label-sm">Name: </label>
                    <div class="col-sm-8">
                        <p class="col-form-label col-form-label-sm">{userInfo.fullName}</p>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-4 col-form-label col-form-label-sm">Score:</label>
                    <div class="col-sm-8">
                        <p class=" col-form-label col-form-label-sm">{userInfo.score}</p>
                    </div>
                </div>
            </form>
        </>
    )
}

export default UserInfo;
