import React, { useEffect, useState } from "react";
import UserInfo from "./UserInfo";

const Board = () => {
    const [board, setBoard] = useState({
        data: [],
        id: null,
        status: 'NEW'
    });

    useEffect(() => {
        const token = localStorage.getItem('userToken')
        const requestOptions = {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`,
            }
        };
        fetch('http://localhost:5000/board/get', requestOptions)
            .then(response => response.json())
            .then(data => setBoard({ data: data.board, id: data.id, status: data.status }))
            .catch((err) => {
                console.log(err)
            })
    }, [])

    const handleChange = (e, i, j) => {
        let regex = /^[1-9]$/;
        let temp = [...board.data];
        if (e.target.value.length <= 1 && regex.test(e.target.value)) {
            temp[i][j] = e.target.value;
            const token = localStorage.getItem('userToken')
            const requestOptions = {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${token}`,
                },
                body: JSON.stringify({ id: board.id, 'sudokuData': temp.flat().join() })
            };
            fetch('http://localhost:5000/board/update', requestOptions)
                .then(response => response.json())
                .then(data => setBoard({ ...board, data: temp, status: data.status }));
        } else {
            temp[i][j] = "";
            setBoard({ ...board, data: temp })
        }
    }
    const handleReset = () => {
        const token = localStorage.getItem('userToken')
        const requestOptions = {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`,
            }
        };
        fetch(`http://localhost:5000/board/reset/${board.id}`, requestOptions)
            .then(response => response.json())
            .then(data => setBoard({ ...board, data: data.board, status: data.status }));
    }
    const handleSave = () => {
        const token = localStorage.getItem('userToken')
        const requestOptions = {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`,
            }
        };
        fetch(`http://localhost:5000/board/save/${board.id}`, requestOptions)
            .then(response => response.json())
            .then(data => setBoard({ ...board, status: data.status }));
    }
    const handleSubmit = () => {
        const token = localStorage.getItem('userToken')
        let sudokuData = board.data.flat().join();
        if (sudokuData.includes(',,')) {
            showColor('red');
            return
        }
        sudokuData = sudokuData.replaceAll('p_', '');
        const requestOptions = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`,
            },
            body: JSON.stringify({ id: board.id, sudokuData })
        };
        fetch(`http://localhost:5000/board/submit/${board.id}`, requestOptions)
            .then(response => response.json())
            .then(data => {
                if (data.isValid) {
                    setBoard({ ...board, status: 'SUBMIT' })
                    showColor('green')
                } else showColor('red')
            });
    }
    const showColor = (color) => {
        let elements = document.getElementsByClassName('elements');
        for (let i = 0; i < elements.length; i++) {
            elements[i].style.backgroundColor = color;
        }
        setTimeout(() => {
            for (let i = 0; i < elements.length; i++) {
                elements[i].style.backgroundColor = '';
            }
        }, 2000);
    }

    const { status } = board;
    const disableReset = (status === 'CHANGED') ? false : true;
    const disableSave = (status === 'CHANGED') ? false : true;
    const disableSubmit = (status === 'NEW' || status === 'SUBMIT') ? true : false
    if (!board.data)
        return <h5>Something went wrong</h5>
    return (
        <>
            <div className={'m-auto'} style={{ height: '455px', width: '455px', border: '2px solid' }}>
                {
                    board.data.map((arr, i) => {
                        return (
                            <div className='row m-auto'>
                                {
                                    arr.map((ele, j) => {
                                        return ele && ele.includes('p_') ?
                                            <input
                                                type="text"
                                                style={{ height: '50px', width: '50px', backgroundColor: 'lightgrey' }}
                                                maxLength={1}
                                                size={1}
                                                disabled={true}
                                                value={ele.split('_')[1]}
                                            /> :
                                            <input
                                                className="elements"
                                                type="text"
                                                style={{ height: '50px', width: '50px' }}
                                                onChange={(e) => { handleChange(e, i, j) }}
                                                maxLength={1}
                                                size={1}
                                                value={ele}
                                            />
                                    })
                                }
                            </div>)
                    })}
                <div className='d-flex my-2' style={{ justifyContent: 'space-around' }}>
                    <button id={'reset'} className='btn btn-sm btn-success' disabled={disableReset} onClick={() => handleReset()}>Reset</button>
                    <button id={'save'} className='btn btn-sm btn-primary' disabled={disableSave} onClick={() => handleSave()}>Save</button>
                    <button id={'submit'} className='btn btn-sm btn-primary' disabled={disableSubmit} onClick={() => handleSubmit()}>Submit</button>
                    <button id={'submit'} className='btn btn-sm btn-primary' disabled={status !== 'SUBMIT'} onClick={() => { window.location.reload() }}>New</button>
                </div>
            </div>
            <UserInfo />
        </>
    )
}

export default Board;
