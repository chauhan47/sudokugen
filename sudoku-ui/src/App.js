import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Board from './components/sudoku/Board';
import { BrowserRouter, Route } from "react-router-dom";
import Login from './components/login/Login';
import Register from './components/login/Register';

function App() {
  return (
    <div className={'d-flex'}>
      <BrowserRouter>
        <Route exact path={['/', '/login']} component={Login} />
        <Route exact path='/register' component={Register} />
        <Route exact path='/play' component={Board} />
      </BrowserRouter>
    </div>
  );
}

export default App;
