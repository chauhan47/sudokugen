const SudokuGenerator = require('../sudoku/generator1');
const BoardRepo = require('../repository/boardRepo')

class menuMaster {

    async getBoard(req, res) {
        try {
            let board = [];
            let result = {}
            const { id, sudokuStr, status } = await BoardRepo.findByUserId(req);
            if (id) {
                let sudokuArr = sudokuStr.split(',');
                while (sudokuArr.length) {
                    board.push(sudokuArr.splice(0, 9))
                }
                result = { id, board, status }
            } else {
                let blankSpaces = Math.floor(Math.random() * 60) + 30;
                const generator = new SudokuGenerator(9, blankSpaces)
                const boardStr = generator.printSudoku()
                const response = await BoardRepo.createBoard(req, boardStr)
                if (response) {
                    result = { id: response.id, board: boardStr, status: response.status }
                }
            }
            return res.status(200).json(result);
        } catch (err) {
            return res.status(500).json({ error: 'Something went wrong' });
        }
    };

    async updateBoard(req, res) {
        try {
            const response = await BoardRepo.updateBoard(req);
            return res.status(200).json({ status: response });
        } catch (err) {
            return res.status(500).json({ error: 'Something went wrong' });
        }
    }

    async resetBoard(req, res) {
        try {
            const response = await BoardRepo.resetBoard(req);
            return res.status(200).json(response);
        } catch (err) {
            return res.status(500).json({ error: 'Something went wrong' });
        }
    }
    async saveBoard(req, res) {
        try {
            const response = await BoardRepo.saveBoard(req);
            return res.status(200).json({ status: response });
        } catch (err) {
            return res.status(500).json({ error: 'Something went wrong' });
        }
    }
    async submitBoard(req, res) {
        try {
            const response = await BoardRepo.submitBoard(req);
            return res.status(200).json({ isValid: response });
        } catch (err) {
            return res.status(500).json({ error: 'Something went wrong' });
        }
    }

}

module.exports = new menuMaster