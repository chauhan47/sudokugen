const { sequelize } = require('../database');
const userRepo = require('../repository/userRepo');

module.exports = {
    register: async (req, res) => {
        try {
            const response = await userRepo.register(req.body);
            return res.status(200).json({ response });
        } catch (err) {
            return res.status(400).json({ error: 'error' });
        }
    },
    // Service for login
    login: async (req, res) => {
        try {
            const response = await userRepo.login(req, res);
            return res.status(200).json({ response });
        } catch (err) {
            return res.status(400).json({ error: 'error' });
        }
    },
    info: async (req, res) => {
        try {
            const response = await userRepo.info(req, res);
            return res.status(200).json({ response });
        } catch (err) {
            return res.status(400).json({ error: 'error' });
        }
    }
}