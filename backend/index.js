const express = require('express');
const db = require('./database');
const cors = require('cors');
const config = require('./config');

(async () => {
	try {
		await db.initDbConnection();
		console.log("Db connected successfully!");
	} catch (error) {
		console.log("Db connection failed with error", error);
	}
})();

const corsOptions = {
	credentials: true,
	origin: [
		'http://localhost:3000'
	]
};

const app = express();
app.use(express.json({limit: '200mb'}));
app.use(express.urlencoded({limit: '200mb', extended: true}));
app.use(cors(corsOptions));


//routes  
app.use('/', require('./routes'));


app.listen(config.port, () => {
	console.log(`server running ${config.port}`);
});


