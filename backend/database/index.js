
const Sequelize = require("sequelize");
const sequelizeTransforms = require('sequelize-transforms');
const config = require("../config")
const fs = require('fs')
// database connection according to server

let sequelize =  new Sequelize(
  config.db_name,
  config.db_user, 
  config.db_password,
  {
    port: config.db_port,
    host: config.db_host,
    dialect: config.db_dialect,
    // dialectOptions: {
    //   ssl: {
    //     ca: fs.readFileSync('./cert/BaltimoreCyberTrustRoot.crt.pem', 'utf8')
    //   }
    // }
  });

module.exports = {
  initDbConnection: async () => {
    return new Promise(async (resolve, reject) => {
      try {
        const resp = await sequelize.authenticate();
        sequelizeTransforms(sequelize);
        resolve(resp)
      } catch (e) {
        reject(e)
      }
    }) 
  },
  sequelize
};

//   module.exports = mysql.createPool({
// 	host: process.env.DB_HOST,
// 	user: process.env.DB_USER,
// 	password: process.env.DB_password,
// 	database: process.env.DB_NAME,
// });
