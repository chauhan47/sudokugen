'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('sudoku_state', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      resetState: {
        type: Sequelize.STRING(1000)
      },
      currentState: {
        type: Sequelize.STRING(1000)
      },
      status:{
        type: Sequelize.ENUM('NEW', 'CHANGED', 'RESET', 'SAVE', 'SUBMIT')
      },
      createdBy: {
        type: Sequelize.BIGINT(11),
        allowNull: false
      },
      updatedBy: {
        type: Sequelize.BIGINT(11),
        allowNull: false
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('sudoku_state');
  }
};