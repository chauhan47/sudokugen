class SudokuGenerator {

    constructor(N, K) {
        this.N = N;
        this.K = K;
        this.SRN = parseInt(Math.sqrt(N));
        this.mat = Array.from(Array(this.N), () => new Array(this.N))
        this.fillValues()
    }

    fillValues = () => {
        this.fillDiagonal();
        this.fillRemaining(0, this.SRN);
        this.removeKDigits();
    }

    fillDiagonal = () => {
        for (let i = 0; i < this.N; i = i + this.SRN) {
            this.fillBox(i, i);
        }
    }
    fillBox = (row, col) => {
        let num;
        for (let i = 0; i < this.SRN; i++) {
            for (let j = 0; j < this.SRN; j++) {
                do {
                    num = this.randomGenerator(this.N);
                }
                while (!this.unUsedInBox(row, col, num));
                this.mat[row + i][col + j] = num;
            }
        }
    }
    randomGenerator = (num) => {
        return Math.floor((Math.random() * num + 1));
    }
    CheckIfSafe = (i, j, num) => {
        return (this.unUsedInRow(i, num) &&
            this.unUsedInCol(j, num) &&
            this.unUsedInBox(i - i % this.SRN, j - j % this.SRN, num));
    }
    unUsedInBox = (rowStart, colStart, num) => {
        for (let i = 0; i < this.SRN; i++)
            for (let j = 0; j < this.SRN; j++)
                if (this.mat[rowStart + i][colStart + j] == num)
                    return false;

        return true;
    }
    unUsedInRow = (i, num) => {
        for (let j = 0; j < this.N; j++)
            if (this.mat[i][j] == num)
                return false;

        return true;
    }
    unUsedInCol = (j, num) => {
        for (let i = 0; i < this.N; i++)
            if (this.mat[i][j] == num)
                return false;
        return true;
    }
    fillRemaining = (i, j) => {
        if (j >= this.N && i < this.N - 1) {
            i = i + 1;
            j = 0;
        }
        if (i >= this.N && j >= this.N)
            return true;

        if (i < this.SRN) {
            if (j < this.SRN)
                j = this.SRN;
        }
        else if (i < this.N - this.SRN) {
            if (j == parseInt(i / this.SRN) * this.SRN)
                j = j + this.SRN;
        }
        else {
            if (j == this.N - this.SRN) {
                i = i + 1;
                j = 0;
                if (i >= this.N)
                    return true;
            }
        }

        for (let num = 1; num <= this.N; num++) {
            if (this.CheckIfSafe(i, j, num)) {
                this.mat[i][j] = num;
                if (this.fillRemaining(i, j + 1))
                    return true;
                this.mat[i][j] = null;
            }
        }
        return false;
    }

    removeKDigits = () => {
        let count = this.K;
        while (count != 0) {
            let cellId = this.randomGenerator(this.N * this.N);

            let i = parseInt(cellId / this.N);
            let j = cellId % 9;
            if (j != 0)
                j = j - 1;
            if (this.mat[i][j] != 0) {
                count--;
                this.mat[i][j] = null;
            }
        }
    }

    printSudoku = () => {
        for (let i = 0; i < this.N; i++)
            for (let j = 0; j < this.N; j++)
                if (this.mat[i][j] > 0)
                    this.mat[i][j] = 'p_' + this.mat[i][j]
        return this.mat;
    }

}

module.exports = SudokuGenerator;