class SudokuChecker {

    constructor(sudokuStr) {
        this.board = [];
        this.boxesChecked = {}
        let sudokuArr = sudokuStr.split(',');
        while (sudokuArr.length) {
            this.board.push(sudokuArr.splice(0, 9))
        }
    }
    // 00 03 06
    // 30 33 36
    // 60 63 66
    CheckIfSafe = (i, j, num) => {
        for (let i = 0; i < 9; i++)
            for (let j = 0; j < 9; j++) {
                if (i === j)
                    if (!this.checkRow(i) || !this.checkColumn(j))
                        return false
                if (i % 3 === 0 && j % 3 === 0)
                    if (!this.checkBox(i, j))
                        return false
            }
        return true
    }
    checkRow = (i) => {
        let obj = { 1: true, 2: true, 3: true, 4: true, 5: true, 6: true, 7: true, 8: true, 9: true }
        for (let j = 0; j < 9; j++)
            delete obj[this.board[i][j]];
        return !Object.keys(obj).length
    }
    checkColumn = (j) => {
        let obj = { 1: true, 2: true, 3: true, 4: true, 5: true, 6: true, 7: true, 8: true, 9: true }
        for (let i = 0; i < 9; i++)
            delete obj[this.board[i][j]];
        return !Object.keys(obj).length
    }
    checkBox = (boxStartI, boxStartJ) => {
        let obj = { 1: true, 2: true, 3: true, 4: true, 5: true, 6: true, 7: true, 8: true, 9: true }
        for (let i = boxStartI; i < boxStartI + 3; i++) {
            for (let j = boxStartJ; j < boxStartJ + 3; j++) {
                delete obj[this.board[i][j]];
            }
        }
        return !Object.keys(obj).length

    }
    unUsedInBox = (rowStart, colStart, num) => {
        for (let i = 0; i < this.SRN; i++)
            for (let j = 0; j < this.SRN; j++)
                if (this.mat[rowStart + i][colStart + j] == num)
                    return false;

        return true;
    }
    unUsedInRow = (i, num) => {
        for (let j = 0; j < this.N; j++)
            if (this.mat[i][j] == num)
                return false;

        return true;
    }
    unUsedInCol = (j, num) => {
        for (let i = 0; i < this.N; i++)
            if (this.mat[i][j] == num)
                return false;
        return true;
    }

    verifySudoku = (sudokuStr) => {
        let board = [];
        let sudokuArr = sudokuStr.split(',');
        while (sudokuArr.length) {
            board.push(sudokuArr.splice(0, 9))
        }
        for (let i = 0; i < this.N; i++)
            for (let j = 0; j < this.N; j++)
                if (!this.CheckIfSafe(i, j, board[i][j]))
                    return false;
        return true
    }

    printSudoku = () => {
        for (let i = 0; i < this.N; i++)
            for (let j = 0; j < this.N; j++)
                if (this.mat[i][j] > 0)
                    this.mat[i][j] = 'p_' + this.mat[i][j]
        return this.mat;
    }

}

module.exports = SudokuChecker;
