class SudokuGenerator {

    constructor() {
        this.board = [
            [null, null, null, null, null, null, null, null, null,],
            [null, null, null, null, null, null, null, null, null,],
            [null, null, null, null, null, null, null, null, null,],
            [null, null, null, null, null, null, null, null, null,],
            [null, null, null, null, null, null, null, null, null,],
            [null, null, null, null, null, null, null, null, null,],
            [null, null, null, null, null, null, null, null, null,],
            [null, null, null, null, null, null, null, null, null,],
            [null, null, null, null, null, null, null, null, null,],
        ]
        this.boardSize = 9;
        this.rndmI = Math.floor(Math.random() * 9);
        this.rndmJ = Math.floor(Math.random() * 9);
        this.rndmVal = Math.floor(Math.random() * 10);
        this.prefilledValuesCount = 2000//Math.floor(Math.random() * 14) + 11;
        this.board[this.rndmI][this.rndmJ] = 'p_' + this.rndmVal
        this.generateRndmNumbers();
    }

    generateWithPreFilled = () => {
        this.generateRndmNumbers();


    }
    checkRow = (obj) => {
        for (let j = 0; j < this.board[0].length; j++) {
            if (this.board[this.rndmI][j]) {
                if (this.board[this.rndmI][j]) {
                    delete obj[this.board[this.rndmI][j]];
                }
            }
        }
    }
    checkColumn = (obj) => {
        for (let i = 0; i < this.board[0].length; i++) {
            if (this.board[i][this.rndmJ]) {
                if (this.board[i][this.rndmJ]) {
                    delete obj[this.board[i][this.rndmJ]];
                }
            }
        }
    }
    checkBox = (obj) => {
        let boxStartI = parseInt(this.rndmI / 3) * 3;
        let boxStartJ = parseInt(this.rndmJ / 3) * 3;
        for (let i = boxStartI; i < boxStartI + 3; i++) {
            for (let j = boxStartJ; j < boxStartI + 3; j++) {
                if (this.board[i][j]) {
                    delete obj[this.board[i][j]];
                }
            }
        }
    }
    fillPreFilled = () => {
        let obj = { 'p_1': true, 'p_2': true, 'p_3': true, 'p_4': true, 'p_5': true, 'p_6': true, 'p_7': true, 'p_8': true, 'p_9': true }
        this.checkRow(obj);
        this.checkColumn(obj);
        this.checkBox(obj);
        let keys = Object.keys(obj);
        if(keys.length){
            let rndmIndex = Math.floor(Math.random() * keys.length);
            this.board[this.rndmI][this.rndmJ] = keys[rndmIndex]
        } else {
            this.rndmI = Math.floor(Math.random() * 9);
            this.rndmJ = Math.floor(Math.random() * 9);
            this.fillPreFilled();
        }
    }

    generateRndmNumbers = () => {
        while (this.prefilledValuesCount) {
            this.rndmI = Math.floor(Math.random() * 9);
            this.rndmJ = Math.floor(Math.random() * 9);
            console.log('val', this.board[this.rndmI][this.rndmJ]);
            console.log(this.rndmI, this.rndmJ);
            if(this.board[this.rndmI][this.rndmJ] === undefined){
                console.log('undefined');
            }
            if (!this.board[this.rndmI][this.rndmJ]) {
                this.prefilledValuesCount--;
                console.log('count', this.prefilledValuesCount);
                this.fillPreFilled();
            }
        }
    }

    getBoard = () => {
        return this.board;
    }
}

module.exports = SudokuGenerator;