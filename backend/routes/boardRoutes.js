const router = require('express').Router();
const controller = require('../controllers');

router.get('/get', controller.getBoard);

router.put('/update', controller.updateBoard);

router.get('/reset/:id', controller.resetBoard);

router.get('/save/:id', controller.saveBoard);

router.post('/submit/:id', controller.submitBoard);


module.exports = router;
