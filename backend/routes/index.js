const express = require('express');
const router = express.Router();
const auth = require('../middleware/auth')

router.use('/user', require('./userRoutes'));
router.use("/board", auth.authUser, require("./boardRoutes"));

module.exports = router;
