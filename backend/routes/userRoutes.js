const router = require('express').Router();
const user = require('../controllers/userController');
const auth = require('../middleware/auth')

router.post('/register', user.register);
router.post('/signin', user.login);
router.get('/info', auth.authUser, user.info);

module.exports = router;
