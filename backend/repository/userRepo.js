const { Op, Sequelize } = require('sequelize');
const { sequelize } = require('../database');
const db = require('../database').sequelize;
const userAuthModel = require('../models/userauthmodel');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const config = require('../config');

// service for regiter
exports.register = async (payload, role) => {
  try {
    payload = payload.userData;
    const salt = bcrypt.genSaltSync();
    payload.encryptedPassword = await bcrypt.hashSync(payload.password, salt);
    delete payload.password;

    const userInfoRes = await userAuthModel.create({
      fullName: payload.fullname,
      userName: payload.username,
      encryptedPassword: payload.encryptedPassword,
    });
    return userInfoRes

  } catch (err) {
    console.log(err);;
    return err
  }
}
// Service for login
exports.login = async (req, res) => {
  const payload = req.body.loginCredentials;
  try {
    const userRes = await userAuthModel.findOne({ // userRes.refUserType = 1
      where: {
        userName: payload.username,
      }
    })

    if (userRes) {
      const isValid = await bcrypt.compare(payload.password, userRes.encryptedPassword);
      if (isValid) {
        const { id, username } = userRes;
        // const { userType } = userRes.userType;
        const token = jwt.sign({ id, username }, config.user_jwt.secret_key, {
          expiresIn: config.user_jwt.expires_in
        })

        return [200, { token }, 'Successfully loggedin'];
      } else {
        return [403, { pass: decryptedPayload.password, enc: userRes.encryptedPassword }, 'Wrong Password'];
      }
    } else {
      return [401, {}, 'User not found'];
    }
  } catch (err) {
    console.log(err);
    return err
  }

}
exports.info = async (req, res) => {
  const payload = req.body.loginCredentials;
  try {
    const userRes = await userAuthModel.findOne({ // userRes.refUserType = 1
      where: {
        id: req.userId,
      }
    })

    if (userRes) {
      const { fullName, score } = userRes;
      return { fullName, score }
    } else {
      return [401, {}, 'User not found'];
    }
  } catch (err) {
    console.log(err);
    return err
  }

}