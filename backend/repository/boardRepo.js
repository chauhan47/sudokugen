const { Op, Sequelize } = require('sequelize');
const { sequelize } = require('../database');
const db = require('../database').sequelize;
const userAuthModel = require('../models/userauthmodel');
const sudokuState = require('../models/sudokustatemodel');
const config = require('../config');
const SudokuChecker = require('../sudoku/checker');

exports.findByUserId = async (req) => {
    try {
        const sudokuRes = await sudokuState.findOne({
            where: {
                createdBy: req.userId,
                status: {
                    [Op.ne]: 'SUBMIT',
                }
            }
        })
        let sudokuStr = '',
            id = null,
            status = 'NEW'
        if (sudokuRes) {
            id = sudokuRes.id
            status = sudokuRes.status
            if (sudokuRes.submitState)
                sudokuStr = sudokuRes.submitState
            else if (sudokuRes.currentState)
                sudokuStr = sudokuRes.currentState
            else if (sudokuRes.resetState)
                sudokuStr = sudokuRes.resetState
        }

        return { id, sudokuStr, status }
    } catch (err) {
        console.log(err);;
        return err
    }
}
exports.createBoard = async (req, board) => {
    try {
        board = board.flat().join();
        const createRes = await sudokuState.create({
            resetState: board,
            status: 'NEW',
            createdBy: req.userId,
            updatedBy: req.userId,
        })

        return createRes
    } catch (err) {
        console.log(err);;
        return err
    }
}
exports.updateBoard = async (req, role) => {
    try {
        const payload = req.body
        const updateRes = await sudokuState.update({
            currentState: payload.sudokuData,
            status: 'CHANGED',
        }, {
            where: {
                id: payload.id
            }
        })
        if (updateRes[0])
            return "CHANGED"
    } catch (err) {
        console.log(err);;
        return err
    }
}

exports.resetBoard = async (req, role) => {
    try {
        const sudoku = await sudokuState.findOne({
            where: {
                id: req.params.id,
            }
        })
        const updateRes = await sudokuState.update({
            currentState: sudoku.resetState,
            status: 'RESET'
        }, {
            where: {
                id: req.params.id
            }
        })
        let board = [];
        let sudokuArr = sudoku.resetState.split(',');
        while (sudokuArr.length) {
            board.push(sudokuArr.splice(0, 9))
        }
        return { board, status: 'RESET' }
    } catch (err) {
        console.log(err);;
        return err
    }
}
exports.saveBoard = async (req, role) => {
    try {
        const sudoku = await sudokuState.findOne({
            where: {
                id: req.params.id,
            }
        })
        const updateRes = await sudokuState.update({
            resetState: sudoku.currentState,
            status: 'SAVE'
        }, {
            where: {
                id: req.params.id
            }
        })
        if (updateRes[0])
            return 'SAVE'
    } catch (err) {
        console.log(err);;
        return err
    }
}
exports.submitBoard = async (req, role) => {
    try {
        const payload = req.body;
        const checker = new SudokuChecker(payload.sudokuData);
        const isValid = checker.CheckIfSafe()
        if (isValid) {
            const result = await sequelize.transaction(async (t) => {
                const updateRes = await sudokuState.update({
                    currentState: payload.sudokuData,
                    status: 'SUBMIT'
                }, {
                    where: {
                        id: req.params.id
                    }
                }, { transaction: t })
                const updateScore = await userAuthModel.increment('score', { by: 1, where: { id: req.userId }}, { transaction: t })
            })
        }
        return isValid
    } catch (err) {
        console.log(err);;
        return err
    }
}