const jwt = require('jsonwebtoken');
const config = require('../config');

exports.authUser = (req, res, next) => {
    if (req.headers) {
        const authHeader = req.headers['authorization'];
        const token = authHeader && authHeader.split(' ')[1]
        if (token == null)
            return res.status(401).json({ error: 'Invalid token' });
        jwt.verify(token, config.user_jwt.secret_key, (err, user) => {
            if (err )
                return res.status(401).json({ error: 'Token Expired' });

            req.userId = user.id;
            next()
        })
    } else {
        return res.status(401).json({ error: 'Invalid header' });
    }

}