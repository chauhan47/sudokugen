const Sequelize = require("sequelize");
const sequelize = require("../database").sequelize;

const menuMaster = sequelize.define('sudoku_state', {
  id: {
    allowNull: false,
    autoIncrement: true,
    primaryKey: true,
    type: Sequelize.INTEGER
  },
  resetState: {
    type: Sequelize.STRING
  },
  currentState: {
    type: Sequelize.STRING
  },
  status: {
    type: Sequelize.ENUM('NEW', 'CHANGED', 'RESET', 'SAVE', 'SUBMIT')
  },
  createdBy: {
    type: Sequelize.INTEGER,
    allowNull: false,
    references: {
      model: 'user_auth',
      key: 'id'
    }
  },
  updatedBy: {
    type: Sequelize.INTEGER,
    allowNull: false,
    references: {
      model: 'user_auth',
      key: 'id'
    }
  },
  createdAt: {
    allowNull: false,
    type: Sequelize.DATE
  },
  updatedAt: {
    allowNull: false,
    type: Sequelize.DATE
  }
},
  {
    freezeTableName: true,
    tableName: 'sudoku_state'
  });



module.exports = menuMaster;