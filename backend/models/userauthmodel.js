const Sequelize = require("sequelize");
const sequelize = require("../database").sequelize;

const userAuthModel = sequelize.define('user_auth', {
  id: {
    allowNull: false,
    autoIncrement: true,
    primaryKey: true,
    type: Sequelize.INTEGER
  },
  fullName: {
    type: Sequelize.STRING
  },
  score: {
    type: Sequelize.INTEGER,
    defaultValue: 0
  },
  userName: {
    type: Sequelize.STRING,
    unique: true
  },
  encryptedPassword: {
    type: Sequelize.STRING
  },
  createdAt: {
    allowNull: false,
    type: Sequelize.DATE
  },
  updatedAt: {
    allowNull: false,
    type: Sequelize.DATE
  }
},
  {
    freezeTableName: true,
    tableName: 'user_auth'
  });



module.exports = userAuthModel;